################################################################################
#
# openocd
#
################################################################################

OPENOCD_VERSION = 0.7.0
OPENOCD_SOURCE = openocd-$(OPENOCD_VERSION).tar.bz2
OPENOCD_SITE = http://downloads.sourceforge.net/project/openocd/openocd/$(OPENOCD_VERSION)

OPENOCD_CONF_ENV = CFLAGS="$(TARGET_CFLAGS) -std=gnu99"
OPENOCD_AUTORECONF = YES
OPENOCD_CONF_OPTS = \
	--oldincludedir=$(STAGING_DIR)/usr/include \
	--includedir=$(STAGING_DIR)/usr/include \
	--enable-ft2232_libftdi  \
	--enable-usbprog  \
	--disable-werror  \
	--disable-doxygen-html

# Rely on the Config.in options of each individual adapter selecting
# the dependencies they need.

OPENOCD_DEPENDENCIES = \
	$(if $(BR2_PACKAGE_LIBFTDI),libftdi) \
	$(if $(BR2_PACKAGE_LIBUSB),libusb) 

# Adapters
OPENOCD_CONF_OPTS += \

# Enable all configuration options for host build.
#
# Note that deprecated options have been removed. CMSIS_DAP needs
# hidapi (currently not included in buildroot) and zy1000 stuff fails
# to build, so they've been removed too.
#
HOST_OPENOCD_CONF_OPTS = \
	--disable-werror  \
	--disable-doxygen-html

HOST_OPENOCD_DEPENDENCIES = host-libftdi host-libusb host-libusb-compat

$(eval $(autotools-package))
$(eval $(host-autotools-package))
